﻿using System;

namespace SelectionSort
{
    /// <summary>
    /// 选择排序
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var array = new[] { 8,6,2,3,1,5,7,4};
            // 选择排序 找出后续数组中最小的那个索引 跟 当前的进行交换
            for (var i = 0; i < array.Length; i++)
            {
                var minIndex = i;
                for (int j = i+1; j < array.Length; j++)
                {
                    if (array[j] < array[minIndex])
                    {
                        minIndex = j;
                    }
                }
                var temp = array[i];
                array[i] = array[minIndex];
                array[minIndex] = temp;
            }

            foreach (var item in array)
            {
                Console.WriteLine(item);
            }
        }
    }
}
