﻿using System;

namespace QuickSort
{
    /// <summary>
    /// 快速排序
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var array = new[] { 8, 6, 2, 3, 1, 5, 7, 4 };
            QuickSort(array);
            foreach (var item in array)
            {
                Console.WriteLine(item);
            }
        }

        public static void QuickSort(int[] arr)
        {
            QuickSort(arr, 0, arr.Length - 1);
        }

        private static void QuickSort(int[] arr, int left, int right)
        {
            if (left >= right)
            {
                return;
            }

            int p = Partition(arr, left, right);
            QuickSort(arr, left, p - 1);
            QuickSort(arr, p + 1, right);
        }

        /// <summary>
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static int Partition(int[] arr, int left, int right)
        {
            //返回值需要符合
            // arr[left...p-1] < arr[p]
            // arr[p+1 ... right] >arr[ p]
            int v = arr[left];
            // 分界点
            int j = left;
            for (int i = left + 1; i < right; i++)
            {
                if (arr[i] < v)
                {
                    var temp = arr[j + 1];
                    arr[j + 1] = arr[i];
                    arr[i] = temp;
                    j++;
                }
            }

            var temp1 = arr[left];
            arr[left] = arr[j];
            arr[j] = temp1;

            return j;
        }
    }
}