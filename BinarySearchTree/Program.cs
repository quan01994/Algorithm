﻿using System;

namespace BinarySearchTree
{
    /// <summary>
    /// 二分搜索树 
    /// 每一个Node 都要符合
    /// 左孩子 小于 顶部
    /// 右孩子 大于 顶部
    /// 没有包含 等于 所以不会存在 重复元素
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var bst = new Bst<int>();
            var nums = new[] {5, 3, 6, 8, 4, 2};
            foreach (var num in nums)
            {
                bst.Add(num);
            }
            bst.PreOrder();
            Console.WriteLine();
            bst.InOrder();
            Console.WriteLine();
            bst.PostOrder();
        }


    }
}
