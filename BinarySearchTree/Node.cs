﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinarySearchTree
{
    class Node<T> 
    {
        public Node(T e)
        {
            E = e;
        }


        public T E { get; set; }

        public Node<T> Right { get; set; }

        public Node<T> Left { get; set; }
    }
}
