﻿using System;

namespace BinarySearchTree
{
    public class Bst<T> where T : IComparable
    {
        private Node<T> _root;

        private int _size;

        public Bst()
        {
            _root = null;
            _size = 0;
        }

        /// <summary>
        /// 添加元素
        /// </summary>
        /// <param name="e"></param>
        public void Add(T e)
        {
            if (_root == null)
            {
                _root = new Node<T>(e);
                _size++;
            }
            else
            {
                Add(_root, e);
            }
        }

        private void Add(Node<T> node, T e)
        {
            if (e.Equals(node.E))
            {
                return;
            }

            if (e.CompareTo(node.E) < 0 && node.Left == null)
            {
                node.Left = new Node<T>(e);
                _size++;
                return;
            }

            if (e.CompareTo(node.E) > 0 && node.Right == null)
            {
                node.Right = new Node<T>(e);
                _size++;
                return;
            }

            if (e.CompareTo(node.E) < 0)
            {
                Add(node.Left, e);
            }
            else
            {
                Add(node.Right, e);
            }
        }

        /// <summary>
        /// 查询 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool Contains(T e)
        {
            return Contains(_root, e);
        }

        private bool Contains(Node<T> node, T e)
        {
            if (node == null)
            {
                return false;
            }

            if (e.CompareTo(node.E) == 0)
            {
                return true;
            }
            else if (e.CompareTo(node.E) < 0)
            {
                Contains(node.Left, e);
            }
            else
            {
                Contains(node.Right, e);
            }

            return false;
        }

        /// <summary>
        /// 前序遍历
        /// 先访问 节点 左孩子 右孩子
        /// </summary>
        public void PreOrder()
        {
            PreOrder(_root);
        }

        private void PreOrder(Node<T> node)
        {
            if (node == null)
            {
                return;
            }

            Console.WriteLine(node.E);

            PreOrder(node.Left);
            PreOrder(node.Right);
        }

        /// <summary>
        /// 中序遍历
        /// 先访问左 访问当前 再访问右
        /// 为有序的
        /// </summary>
        public void InOrder()
        {
            InOrder(_root);
        }

        private void InOrder(Node<T> node)
        {
            if (node == null)
            {
                return;
            }

            InOrder(node.Left);
            Console.WriteLine(node.E);
            InOrder(node.Right);
        }


        /// <summary>
        /// 中序遍历
        /// 先访问左 访问当前 再访问右
        /// </summary>
        public void PostOrder()
        {
            PostOrder(_root);
        }

        private void PostOrder(Node<T> node)
        {
            if (node == null)
            {
                return;
            }

            PostOrder(node.Left);
            PostOrder(node.Right);
            Console.WriteLine(node.E);
        }
    }
}