﻿using System;

namespace LongestIncreasingSubsequence
{
    class Program
    {
        /*
         * 你有一个不同整数的列表{x1，x2，...，xn}。 给出一个算法来计算列表中最长的子序列的基数。
         * 例如：给定列表{12,3,8,7,5,6,13,10,11,4}，最长的增加子序列是{3,5,6,10,11}，基数为5
         */
        static void Main(string[] args)
        {
            var nums = new int[] {12, 3, 8, 7, 5, 6, 13, 10, 11, 4};
            int[] dp = new int[nums.Length];
            int len = 0;
            foreach (var num in nums)
            {
                int i = Array.BinarySearch(dp, 0, len, num);
                if (i < 0)
                {
                    i = -(i + 1);
                }

                if (len == i || len - 1 == i)
                {
                    dp[i] = num;
                }

                if (i == len)
                {
                    len++;
                }
            }

            Console.WriteLine(len);
        }
    }
}