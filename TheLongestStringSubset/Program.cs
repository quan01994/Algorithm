﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TheLongestStringSubset
{
    class Program
    {
        /*
         *
         * 给定一个字符串，找到最长子字符串的长度而不重复字符。
         * 例子：
         * 鉴于  "abcabcbb"，答案是  "abc"，长度为3。
         * 给出  "bbbbb"答案是  "b"，长度为1。
         * 给定  "pwwkew"，答案是  "wke"，长度为3.请注意，答案必须是 子字符串，"pwke" 是 子序列而不是子字符串。
         */
        static void Main(string[] args)
        {
            var s = "abcabcbb";
            var ls = new List<char>();


            int intMaxLength = 0;

            for (int i = 0; i < s.Length; i++)
            {
                if (ls.Contains(s[i]))
                {
                    ls.RemoveRange(0, ls.IndexOf(s[i]) + 1);
                }
                ls.Add(s[i]);

                // Console.WriteLine(JsonConvert.SerializeObject(ls));
                intMaxLength = ls.Count > intMaxLength ? ls.Count : intMaxLength;
            }
//            Console.WriteLine(JsonConvert.SerializeObject(ls));
            Console.WriteLine(intMaxLength);
        }


    }
}
