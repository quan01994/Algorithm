﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
//            var xml = File.ReadAllText("test.xml");
            using (var stream = File.OpenRead("test.xml"))
            using (var xmlReader = XmlReader.Create(stream))
            {
                var xmlSerializer = new XmlSerializer(typeof(Ufinterface));
                var result = xmlSerializer.Deserialize(xmlReader) as Ufinterface;
                Console.WriteLine(result?.Result.WaybillProcessInfos.Count);
            }
        }
    }

    [XmlRoot("ufinterface")]
    public class Ufinterface
    {
        [XmlElement("Result")] public Result Result { get; set; }
    }

    public class Result
    {
        public Result()
        {
            WaybillProcessInfos = new List<WaybillProcessInfo>();
        }

        [XmlElement("WaybillProcessInfo")] public List<WaybillProcessInfo> WaybillProcessInfos { get; set; }
    }

    public class WaybillProcessInfo
    {
        [XmlElement("Waybill_No")] public string WaybillNo { get; set; }
    }
}
