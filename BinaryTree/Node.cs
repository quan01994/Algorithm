﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    class Node<T>
    {
        public T E { get; set; }

        public Node<T> Left { get; set; }

        public Node<T> Right { get; set; }
    }
}
