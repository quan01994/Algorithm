﻿using System;

namespace MergeSort
{
    /// <summary>
    /// 归并排序  合并排序
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var arr = new[] {8, 6, 2, 3, 1, 5, 7, 4};

            MergeSort(arr, 0, arr.Length - 1);
//            MergeSort(arr);
            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }
        }

        /// <summary>
        /// 对arr [ l ... r] 排序
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        private static void MergeSort(int[] arr, int left, int right)
        {
            // 其实这里可以优化成插入排序
            // right - left <= 15 的时候就可以用插入排序
            if (left >= right)
            {
                return;
            }
            // 有可能会溢出
            int mid = (left + right) / 2;
            MergeSort(arr, left, mid);
            MergeSort(arr, mid + 1, right);
            // 假设 mid mid+1 是有序的
            if (arr[mid] > arr[mid + 1])
            {
                Merge(arr, left, mid, right);
            }
        }

        private static void Merge(int[] arr, int left, int mid, int right)
        {
            var aux = new int[right - left + 1];
            for (int index = left; index <= right; index++)
            {
                aux[index - left] = arr[index];
            }

            //
            int i = left;
            int j = mid + 1;
            for (int k = left; k <= right; k++)
            {
                if (i > mid)
                {
                    arr[k] = aux[j - left];
                    j++;
                }

                else if (j > right)
                {
                    arr[k] = aux[i - left];
                    i++;
                }

                else if (aux[i - left] < aux[j - left])
                {
                    arr[k] = aux[i - left];
                    i++;
                }
                else
                {
                    arr[k] = aux[j - left];
                    j++;
                }
            }
        }
    }
}
