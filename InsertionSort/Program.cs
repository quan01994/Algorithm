﻿using System;

namespace InsertionSort
{
    /// <summary>
    /// 插入排序
    /// </summary>
    class Program
    {
        // 插入排序
        // 从第二个元素开始比较，
        // 后面每个元素，依次与前一个元素进行比较，如果小于前一个元素，就与之交换位置，

        // 还有一种优化，第二个值先取出。跟第一个值比较。如果大于，第一个值就跟交换
        // 第三个值去出，与第二个值进行比较， 如果3<2 3与2交换
        static void Main(string[] args)
        {
            var array = new[] {8, 6, 2, 3, 1, 5, 7, 4};

            for (int i = 1; i < array.Length; i++)
            {
                var e = array[i];
                int j = i; //保存e 存放的位置
                for ( ; j > 0 && array[j - 1] > e; j--)
                {
                    array[j] = array[j - 1];
                }

                array[j] = e;
            }

            foreach (var item in array)
            {
                Console.WriteLine(item);
            }
        }
    }
}